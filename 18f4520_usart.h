struct _txsta{						//Transmit Status and Control Register
	int TX9D:1;						//9th bit of transmit data
	int TRMT:1						//transmit shift register status bit: 1=TSR empty, 0=TSR full
	int BRGH:1;						//high baud rate select bit: 1=high, 0=low
	int SENDB:1;					//send break character bit: asynch mode only 1=send break, 0=send break completed
	int SYNC:1;						//eusart mode select bit: 1=synchronous, 0=asynchronous
	int TXEN:1;						//transmit enable bit: 1=tx enabled, 0=tx disabled
	int TX9:1;						//9-bit transmit enable bit: 1=9 bit tx, 0=8 bit tx
	int CSRC:1;						//clock source select bit: synch mode only 1=master mode (internal clock), 0=slave mode (external clock)
};
struct _txsta *TXSTA = 0xFAC;

struct _rcsta{						//Receive Status and Control Register
	int RX9D:1;						//9th bit of received data
	int OERR:1;						//overrun error bit: 1=overrun error (clear CREN to clear), 0=no overrun error
	int FERR:1;						//framing error bit: 1=framing error (update by reading RCREG and receiving next valid byte)
	int ADDEN:1;					//address detect enable bit: asycnh only 1=enables address detection, 0=address detection disabled
	int CREN:1;						//continuous receive enable bit: asynch mode 1=enables receiver, 0=disables receiver; synch mode 1=enabled until CREN is cleared 0=disabled
	int SREN:1;						//single receive enable bit synch mode only 1=enables single receive, 0=disabled
	int RX9:1;						//9 bit receive enable bit: 1=9 bit reception, 0=8bit reception
	int SPEN:1;						//serial port enable bit: 1=enabled (sets RX/DT & TX/CK pins as serial port pins)
};
struct _rcsta *RCSTA = 0xFAB;


struct _baudcon{					//Baud Rate Control Register
	int ABDEN:1;					//auto-baud detect enable bit: asynch only 1=enable baud rate detection on next character, cleared upon completion 0=disabled or completed
	int WUE:1;						//wake up enable bit: asynch mode only 1=enabled, 0=disabled
	int nc1:1;						//unimplemented
	int BRG16:1;					//16 bit baud rate register enable bit: 1=16 bit baud rate generator, 0=8 bit baud rate generator
	int SCKP:1;						//synchronous clock polarity select bit: synch mode only 1=idle state for clock is a high level, 0=idle state for clock is low
	int nc2;						//unimplemented
	int RCIDL:1;					//receive operation idle status bit: 1=receive operation is idle, 0=receive operation is active
	int ABDOVF:1;					//auto baud acquisition rollover status bit: 1=a BRG rollover has occurred during auto baud detection, 0=no BRG rollover has occurred
};
struct _baudcon *BAUDCON = 0xFB8;


