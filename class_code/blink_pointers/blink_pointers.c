#include <18f4520.h>
#use delay (clock=20000000)
#fuses HS, NOWDT, NOLVP

int *PORTC = 0xF82, *TRISC = 0xF94;

main(){
   *TRISC = 0x00;
   *PORTC = 0x00;                            //Bypass
   while(1){
      *PORTC ^= 0x01;
      delay_ms(500);
   }
}
