#include <18f4520.h>
#use delay (clock=20000000)
#fuses HS, NOWDT, NOLVP

struct myPort{
   int LOW:4;
   int HIGH:4;
};
struct myPort *PORTC = 0xF82;

int *TRISC = 0xF94;

main(){
   *TRISC = 0x00;
   PORTC->LOW=0;
   PORTC->HIGH=15;
   while(1){
      PORTC->LOW = PORTC->LOW + 1;
      PORTC->HIGH = PORTC->HIGH - 1;
      delay_ms(500);
   }
}
