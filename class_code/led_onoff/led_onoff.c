#include <18f4520.h>
#use delay (clock=20000000)
#fuses HS, NOWDT, NOLVP

main(){
   //SETUP INSTRUCTIONS - executed once
   set_tris_c(0x00);                      //ALL 8 BITS ARE OUTPUT
   output_c(0x00);                        //ALL OUTPUTS SET TO 0; MAKES SURE THERE IS NOTHING LEFTOVER FROM PREVIOUS CODE; CLEANS IT UP
   while(1){
      //PROGRAM
      output_toggle(PIN_C0);
      delay_ms(500);
   }
}




