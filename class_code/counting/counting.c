#include <18f4520.h>
#use delay (clock=20000000)
#fuses HS, NOWDT, NOLVP

main(){
   int x = 0;
   set_tris_c(0x00);
   output_c(0x00);
   while(1){
      output_c(x);
      delay_ms(500);
      x++;
   }
}
