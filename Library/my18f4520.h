//pointer to GPIOs

#define *TRISA = 0xF92;
#define *TRISB = 0xF93;
#define *TRISC = 0xF94;
#define *TRISD = 0xF95;

#define *PORTA = 0xF80;
#define *PORTB = 0xF81;
#define *PORTC = 0xF82;
#define *PORTD = 0xF83;

//Analog to Digital Conv Circuits
struct _adcon1{
   int PCFGx:4;
   int VCFG0:1;
   int VCFG1:1;
   int unused:2;
};
struct _adcon1 *ADCON = 0xFC1;

