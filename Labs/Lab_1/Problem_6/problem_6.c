#include <problem_6.h>
#use delay (clock = 20000000)
#fuses HS, NOWDT, NOLVP
#include "../../Library/my18f4520.h"

struct _myPort{                  //create a structure named myPort
   int B0:1;                     //port B0
   int led:1;                    //port B1 is the led port
   int B2_B7:6;                  //we are not using these ports for this code, so they are grouped together
};
struct _myPort *MYPORT = 0xF81;  //connect the struct to the hardware   

main(){
   ADCON -> PCFGx = 0xF;        //PORTB Digital
   *TRISB = 0x00;                //Port B all pins outputs
   MYPORT->led = 1;              //make led value 1
     
   while(1){
      MYPORT->led = MYPORT->led ^ 1;
      delay_ms(500);
   }
}
   

