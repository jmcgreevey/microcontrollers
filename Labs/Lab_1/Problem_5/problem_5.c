#include <problem_5.h>
#include "../../Library/my18f4520.h"
//fuses and 18f4520.h library included in first header file

void main()
{

   *TRISC = 0x00;
   *PORTC = 0x00;
   while(1)
   {
      *PORTC^=0x01;
      delay_ms(500);
   }

}
