#include <problem7_no_struct.h>
//#include "../../Library/my18f4520.h"

struct _adcon1{
   int PCFGx:4;
   int VCFG0:1;
   int VCFG1:1;
   int unused:2;
};
struct _adcon1 *ADCON = 0xFC1;

int *TRISC = 0xF94;
int *PORTC = 0xF82;

void main()
{
   *TRISC = 0x01;
   
   ADCON->PCFGx = 0xF;
   
   while(TRUE)
   {
      if(*PORTC & 0x01){
         *PORTC |= 0x80;
      }
      else{
          *PORTC &= ~0x80;
      }
   }

}
